from django.contrib import admin
from django.conf.urls import url, include


API_PATH = 'api/v1'

schema_view = get_swagger_view(title='Miles & Tips Core API')

admin.autodiscover()

urlpatterns = [
    url('^$', schema_view),
    url(r'^auth/', include('djoser.urls')),
    url(r'^auth/', include('djoser.urls.jwt')),
    url(r'^{}/'.format(API_PATH), include(users_router.urls)),
    url(r'^{}/'.format(API_PATH), include(user_addresses_router.urls)),
    url(r'^{}/'.format(API_PATH), include(user_contacts_router.urls)),
    url(r'^{}/'.format(API_PATH), include(user_loyalty_programs_router.urls)),
    url(r'^{}/'.format(API_PATH), include(points_balance_router.urls)),
    url(r'^{}/'.format(API_PATH), include(subscriptions_router.urls)),
    url(r'^{}/'.format(API_PATH), include(subscriptions_payment_router.urls)),
    url(r'^{}/'.format(API_PATH), include(loyalty_program_router.urls)),
    url(r'^{}/'.format(API_PATH), include(plan_router.urls)),
    url(r'^{}/'.format(API_PATH), include(tip_router.urls)),
    url(r'^retorno/pagseguro/', include('pagseguro.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
