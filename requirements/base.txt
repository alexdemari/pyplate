django==1.11.6
django-cors-headers==2.1.0
django-environ==0.4.4
django-extensions==1.9.1
django-model-utils==3.0.0
django-rest-swagger==2.1.2
djangorestframework==3.7.0
djangorestframework-jwt==1.11.0
dj-database-url== 0.4.2
drf-nested-routers==0.90.0
dry-rest-permissions==0.1.9
psycopg2==2.7.3.1
python-dateutil==2.6.1
whitenoise==3.3.1
drf-tracking==1.4.0
