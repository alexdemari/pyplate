.DEFAULT_GOAL := default_target
.PHONY: default_target test clean setup create-venv setup-dev setup-os git-up git-add-migrations migrations translate-up translate-compile migrations-up migrations-clean migrations-from-scratch db-up heroku-clean heroku-create-production-app heroku-create-staging-app heroku-destroy-apps heroku-destroy-staging-app heroku-destroy-production-app heroku-create heroku-create-staging-app-from-scratch heroku-create-production-app-from-scratch code-convention test run all

NPROC := `nproc --all`
PYTEST := py.test -n$(NPROC)
PIP := pip install -r
APP :=

PYTHON_SITE_PACKAGES_PATH := `python -c "import site; print(site.getsitepackages()[0])"`

ADMIN_URL := `openssl rand -base64 48`
SECRET_KEY := `bash utility/generate-secret-key.sh`

PROJECT_NAME := myproject
PYTHON_VERSION := 3.6.4
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

# Environment setup
.pip:
	pip install pip --upgrade

setup: .pip
	$(PIP) requirements.txt

setup-dev: .pip
	$(PIP) requirements/local.txt

setup-os:
	bash utility/install-docker.sh
	bash utility/install-gitlab-runner.sh

.clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr reports/
	rm -fr .pytest_cache/

clean: .clean-build .clean-pyc .clean-test ## remove all build, test, coverage and Python artifacts

.create-venv:
	pyenv install -s $(PYTHON_VERSION)
	pyenv uninstall -f $(VENV_NAME)
	pyenv virtualenv $(PYTHON_VERSION) $(VENV_NAME)
	pyenv local $(VENV_NAME)

create-venv: .create-venv setup-dev

# Repository
git-up:
	git pull
	git fetch -p --all

git-add-migrations:
	git add myproject/???/migrations

# Translation
translate-up:
	python manage.py makemessages -l pt_BR --symlinks

translate-compile:
	python manage.py compilemessages

# Database
migrations:
	python manage.py makemigrations $(APP)

migrations-up:
	make migrations APP=???

migrations-clean:
	rm -rf mat_core/???/migrations

migrations-from-scratch: migrations-clean migrations-up git-add-migrations

.db-up:
	python manage.py migrate

.db-createsuperuser:
	python manage.py createsuperuser

db-up: .db-up .db-createsuperuser

collectstatic:
	python manage.py collectstatic --noinput

code-convention:
	flake8
	pycodestyle

test:
	$(PYTEST) --cov-report=term-missing  --cov-report=html --cov=.

run:
	python manage.py runserver 0.0.0.0:8000

all: create-venv git-up setup-dev default_target

default_target: clean test code-convention
