# Automation tool

This project uses `Makefile` as automation tool. Replace `myproject` label to your project name.

# Set-up Virtual Environment

The following commands install and set-up `pyenv` tool (https://github.com/pyenv/pyenv) used to create/manage virtual environments:

```bash
$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.bashrc
$ exec "$SHELL"
$ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
$ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
$ exec "$SHELL"
```

After that, access the project directory and execute `make all` to recreate the virtual environment.

# Application settings (Production mode)

There are some variables that need to be set as environment variables or in a `.env` file on project root directory as follow.

```
# PostgreSQL
POSTGRES_PASSWORD=postgrespassword
POSTGRES_USER=postgresuser

# General settings
DJANGO_ADMIN_URL=
DJANGO_SETTINGS_MODULE=config.settings.production
DJANGO_SECRET_KEY=(CHANGE ME!!!)
DJANGO_ALLOWED_HOSTS=127.0.0.1,localhost,0.0.0.0,myproject.com
DJANGO_CORS_ORIGIN_REGEX_WHITELIST='^(http?://)?(\w+\.)?localhost:4200\.*$', r'^(http?://)?(\w+\.)?127.0.0.1:4200\.*$'

# Used with email
DJANGO_MAILGUN_API_KEY=
DJANGO_SERVER_EMAIL=
MAILGUN_SENDER_DOMAIN=

# Security! Better to use DNS for this task, but you can use redirect
DJANGO_SECURE_SSL_REDIRECT=False

```


# Secret Key

There is a tool to generate a new secret key. Execute the following command:

```
$ bash utility/generate_secret_key.sh
```

Use the result to set your local SECRET_KEY variable in `.env` file.

# Database configuration

First it is necessary to set a password for user postgres:
```
$ sudo -u postgres psql postgres
# \password postgres
# postgres
# \q
```

With a username an password set the next step is to create a schema for database:
```
$ sudo -u postgres createdb myproject
```

## Steps to create tables on database and a super user
```
$ make db-up
```

## Steps to create a new migration
```
$ make migrations APP=<app name>
$ make migrate-db
```

## Steps to create migrations from scratch
```
$ make migrations-from-scratch
```
